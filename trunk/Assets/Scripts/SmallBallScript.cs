﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBallScript : MonoBehaviour {

	public Animator animator;
	// Use this for initialization
	public void PlayAnim()
	{
		int idAnim = Random.Range(1,4);
		if(animator != null && gameObject.activeSelf)
		{
			animator.SetInteger("SmallBallAnim", idAnim);
		}
	}
}
