﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoScript : MonoBehaviour {

	// Use this for initialization
	float timeLogo;
	bool changeScence = false;
	void Start () {
		timeLogo = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
		if(!changeScence && Time.timeSinceLevelLoad - timeLogo > Defines.LOGO_TIME)
		{
			changeScence = true;
			GameManager.Instance.LoadScene("GameScene");
		}
	}
}
