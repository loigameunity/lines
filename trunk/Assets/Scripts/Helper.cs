﻿using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Security.Cryptography;
using System;

public static class Helper {


    public static string Serialize<T>(this T objectToSerialize)
    {
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        MemoryStream memStr = new MemoryStream();

        try
        {
            bf.Serialize(memStr, objectToSerialize);
            memStr.Position = 0;

            return Convert.ToBase64String(memStr.ToArray());
        }
        finally
        {
            memStr.Close();
        }
    }

    public static T Deserialize<T>(this string objectToDerialize)
    {
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        byte[] byteArray = Convert.FromBase64String(objectToDerialize);
        MemoryStream memStr = new MemoryStream(byteArray);

        try
        {
            return (T)bf.Deserialize(memStr);
        }
        finally
        {
            memStr.Close();
        }
    }
    /*
    //Serialization
    public static string Serialize<T>(this T toSerialize)
	{
		XmlSerializer xml = new XmlSerializer (toSerialize.GetType());
		StringWriter writer = new StringWriter ();
		xml.Serialize (writer, toSerialize);
		return writer.ToString ();
	}

	// De-serialize

	public static T Deserialize<T>(this string toDeserialize)
	{
		XmlSerializer xml = new XmlSerializer (toDeserialize.GetType());
		StringReader reader = new StringReader (toDeserialize);
		return (T)xml.Deserialize(reader);
	}
    */
	private static string hash = "123987@!abc";

	public static string Encypt(string input)
	{
		byte[] data = UTF8Encoding.UTF8.GetBytes(input);
		using(MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
		{
			byte[] key = md5.ComputeHash (UTF8Encoding.UTF8.GetBytes(hash));
			using (TripleDESCryptoServiceProvider trip = new TripleDESCryptoServiceProvider () {
				Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7
			}) {
				ICryptoTransform tr = trip.CreateEncryptor();
				byte[] result = tr.TransformFinalBlock (data, 0, data.Length);
				return Convert.ToBase64String (result, 0, result.Length);
			}
		}
	}

	public static string Decypt(string input)
	{
		byte[] data = Convert.FromBase64String(input);
		using(MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
		{
			byte[] key = md5.ComputeHash (UTF8Encoding.UTF8.GetBytes(hash));
			using (TripleDESCryptoServiceProvider trip = new TripleDESCryptoServiceProvider () {
				Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7
			}) {
				ICryptoTransform tr = trip.CreateDecryptor();
				byte[] result = tr.TransformFinalBlock (data, 0, data.Length);
				return UTF8Encoding.UTF8.GetString(result);
			}
		}
	}
}
