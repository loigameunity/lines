﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defines : MonoBehaviour {

    public const int TARGET_FPS = 60;
	public const int LOGO_TIME = 3;
	public const int BOARD_COL = 9;
	public const int BOARD_ROW = 9;
	public const int TOTALCELL = 81;
	public const int INIT_BALL_NUM = 5;
	public const int EAT_BALL_LINE_NUM = 5;
	public const int NEXT_BALL_NUM = 3;
	public const int RANDOM_BALL = -1;
	public const int MAX_COLOR = 7;
    public const int BLOCK_LOCK = 8;
    public const int GHOST_BALL_COLOR = 9;
    public const int MAX_LOCK_CELL = 15;
    public const int MIN_LOCK_CELL = 0;
    public const int VALUE_CHANGE_LOCK_CELL = 3;
    
    public const int CHECK_ADD_GHOST_BALL = 6;
    
	public const int MODE_LINES = 0;
	public const int MODE_SQUARE = 1;
	public const int DIALOG_TYPE_NEWGAME = 0;
	public const int DIALOG_TYPE_EXIT = 1;
	public const int DIALOG_TYPE_MODE = 2;
    public const int DIALOG_TYPE_CHANGE_LOCK_CELL = 3;
    public const string DIALOG_CHANGE_LOCK_CELL = "Change Lock Cell Will Lost Data, Are You Sure?";
    public const string DIALOG_NEWGAME = "New Game Will Lost Data, Are You Sure?";
	public const string DIALOG_EXIT = "Save And Exit The Game, Are You Sure?";
	public const string DIALOG_CHANGE_MODE_LINES = "Change Mode Lines Will Lost Data, Are You Sure?";
	public const string DIALOG_CHANGE_MODE_SQUARE = "Change Mode Square Will Lost Data, Are You Sure?";

	public const int TOTAL_HIGHSCORE_RMS = 10;
	public const int NO_DATA_VALUE = 1000;
    public const int MAX_SPEED_BALL = 100;
	// Sound
	public const float MAX_SOUND = 1;
	public const float MIN_SOUND = 0;
	public const int MAX_CHANEL_SOUND = 6;
	public const int SFX_MENU = 0;
	public const int SFX_SELECT_BALL = 1;
	public const int SFX_SELECT_CELL = 2;
	public const int SFX_BREAK_BALL = 3;
	public const int SFX_WARNING = 4;
    public const float TIME_DELAY_SOUND_EARN_SCORE = 0.15f;

	public const int MAX_LEVEL_GAME = 5;
	public const int MIN_LEVEL_GAME = 1;
	public static int[] CONDITION_LEVEL = new int[MAX_LEVEL_GAME]{4, 3, 2, 1, 0};
	public const int MAX_RETRY_RANDOM_BALL = 15;
	public const int MIN_CELL_BLANK = 10;

    //Index Cache Object
    public const int DEFAULT_TOTAL_CACHE = 5;
    public const int INDEX_CELL_CACHE = 0;
    public const int INDEX_HIGHSCORE_CACHE = 1;

	//Anim Ball
	public const int ANIM_BALL_GROWN_UP = 0;
	public const int ANIM_IDLE_01 = 1;
	public const int ANIM_IDLE_02 = 2;
	public const int ANIM_IDLE_03 = 3;
	public const int ANIM_IDLE_04 = 4;
	public const int ANIM_IDLE_05 = 5;
	public const int ANIM_BALL_SELECT = 6;
	public const int ANIM_EAT_BALL = 7;
    public const int ANIM_BLOCK = 8;
}
