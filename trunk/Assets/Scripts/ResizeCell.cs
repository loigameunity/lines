﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResizeCell : MonoBehaviour {

	public GameObject container;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float width = container.GetComponent<RectTransform>().rect.width;
		float height = container.GetComponent<RectTransform>().rect.height;
		Vector2 newSize = new Vector2(width/9, height/9);
		container.GetComponent<GridLayoutGroup>().cellSize = newSize;
	}
}
