﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour {

	public Animator animator;
	// Use this for initialization
	private int animIndex = 0;
    float timeInvoke = 0;
    void Update()
	{
        timeInvoke += Time.deltaTime;
        if (timeInvoke >= 10)
        {
            timeInvoke = 0;
            RepeatAnimIdle();
        }
	}

	private void RepeatAnimIdle()
	{
        if (animIndex < Defines.ANIM_BALL_SELECT)
            RandomAnimIdle();
	}

	public void ResetIndexAnim()
	{
        if (animIndex != Defines.ANIM_BALL_SELECT && animIndex != Defines.ANIM_BLOCK)
		    animIndex = 0;
	}

	public void RandomAnimIdle()
	{
        CancelInvoke();
        animIndex = Random.Range(Defines.ANIM_IDLE_01, Defines.ANIM_BALL_SELECT);
		PlayBallAnim(animIndex, 0);

	}
	public void PlayBallAnim(int idAnim, int idColor)
	{
        DeActiveEffectExplose();
        animIndex = idAnim;
        if (gameObject.activeSelf)
            animator.SetInteger("BallAnim", animIndex);
        else if (animIndex == Defines.ANIM_BLOCK)
        {
            Invoke("DelayPlayAnim",0.1f);
        }

		if(animIndex == Defines.ANIM_EAT_BALL)
		{
            PlayEffectParticle(idColor -1);
            Invoke("DeactiveGameObject",2);
		}
	}

    public void DeActiveEffectExplose()
    {
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void PlayEffectParticle(int id)
    {
        transform.GetChild(id).gameObject.SetActive(true);
    }

    private void DelayPlayAnim()
    {
        if (gameObject.activeSelf)
            animator.SetInteger("BallAnim", animIndex);
    }

    public int GetAnimIndexBall()
    {
        return animIndex;
    }

	private void DeactiveGameObject()
	{
        animIndex = Defines.ANIM_BALL_GROWN_UP;
        gameObject.SetActive(false);
	}
	

}
