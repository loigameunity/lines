﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    public static ObjectPooler Instance;

    private void Awake()
    {
        if (Instance != null)
            return;

        Instance = this;
    }

    private const int defaultPoolSize = Defines.DEFAULT_TOTAL_CACHE;
    public bool expandWhenNecessary = true;
    public Dictionary<GameObject, List<GameObject>> objectPools = new Dictionary<GameObject, List<GameObject>>();
        
    private bool PoolExistsForPrefab(GameObject gameobj)
    {
        return objectPools.ContainsKey(gameobj);
    }

    private bool IsAvailableForReuse(GameObject gameObject)
    {
        return !gameObject.activeSelf;
    }

    private GameObject ExpandPoolAndGetObject(GameObject gameobj, List<GameObject> pool)
    {
        if(!expandWhenNecessary)
            return null;

        GameObject inst = GameObject.Instantiate(gameobj);
        pool.Add(inst);
        return inst;
    }

    public List<GameObject> CreateObjectPool(GameObject gameobj, int count)
    {
        if (count <= 0)
            count = 1;

        List<GameObject> objects = new List<GameObject>();
        for (int i = 0; i < count; i++)
        {
            GameObject inst = GameObject.Instantiate(gameobj); ;
            objects.Add(inst);
            inst.SetActive(false);
        }

        objectPools.Add(gameobj, objects);

        return objects;
    }

    public GameObject GetPooledObject(GameObject gameobj, int poolSize = defaultPoolSize)
    {
        if (!PoolExistsForPrefab(gameobj))
        {
            return CreateAndRetrieveFromPool(gameobj, poolSize);
        }

        var pool = objectPools[gameobj];
        GameObject inst;
        return (inst = FindFirstAvailablePooledObject(pool)) != null ? inst : ExpandPoolAndGetObject(gameobj, pool);
    }

    private GameObject CreateAndRetrieveFromPool(GameObject gameobj, int poolSize = defaultPoolSize)
    {
       CreateObjectPool(gameobj, poolSize);

        return GetPooledObject(gameobj);
    }

    private GameObject FindFirstAvailablePooledObject(List<GameObject> pool)
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (pool[i] != null)
            {
                if (IsAvailableForReuse(pool[i]))
                {
                    return pool[i];
                }
            }
            else
            {
                // Check where destroy object cache pool
                pool.RemoveAt(i);
            }
        }

        return null;
    }
    
}
