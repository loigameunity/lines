﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public List<GameObject> listObjectsCache = new List<GameObject>();
    public List<Material> listMaterial = new List<Material>();
    private RMSData data;
    private string nameRMS = "RMS_LINES";
    private string nextScene = "";
    private bool useObjectPool = true;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        DontDestroyOnLoad(gameObject);
    }   // Use this for initialization
    void Start() {
        Application.targetFrameRate = Defines.TARGET_FPS;
        //	PlayerPrefs.DeleteAll();
        LoadData();
    }

    public void LoadScene(string scene)
    {
        nextScene = scene;
        StartCoroutine("ChangeScene");
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(gameObject.GetComponent<Fading>().BeginFade(1));
        SceneManager.LoadScene(nextScene);
    }

    public void LoadData() {
        if (PlayerPrefs.HasKey(nameRMS)) {
            data = Helper.Deserialize<RMSData>(Helper.Decypt(PlayerPrefs.GetString(nameRMS)));
        } else {
            data = new RMSData();
            InitData();
            SaveData();
        }
    }

    private void InitData()
    {
        for (int i = 0; i < Defines.TOTAL_HIGHSCORE_RMS; i++)
        {
            data.topTenScore.Add(0);
            data.matrix[0] = Defines.NO_DATA_VALUE;
            data.score = 0;
            data.currMode = Defines.MODE_LINES;
            data.soundVolume = 1;
        }
    }
    public void SaveData()
    {
        PlayerPrefs.SetString(nameRMS, Helper.Encypt(Helper.Serialize<RMSData>(data)));
    }

    public int GetCurrMode()
    {
        return data.currMode;
    }
    public void SetCurrMode(int value)
    {
        data.currMode = value;
    }

    public float GetHighScore()
    {
        return data.topTenScore[0];
    }

    public float GetHighScore(int i)
    {
        return data.topTenScore[i];
    }

    public void SetHighScore(float value)
    {
        data.topTenScore.Add(value);
        data.topTenScore.Sort((v1, v2) => v2.CompareTo(v1));
        // Only get 10 highScore
        int count = data.topTenScore.Count;
        if (count > Defines.TOTAL_HIGHSCORE_RMS)
            data.topTenScore.RemoveRange(Defines.TOTAL_HIGHSCORE_RMS, count - Defines.TOTAL_HIGHSCORE_RMS);
    }

    public float GetSoundVolume()
    {
        return data.soundVolume;
    }
    public void SetSoundVolume(float value)
    {
        data.soundVolume = value;
        AudioManager.Instance.UpdateVolumeMusic();
    }

    public int GetValueIndexMatrix(int index)
    {
        return data.matrix[index];
    }

    public void SetValueIndexMatrix(int index, int value)
    {
        data.matrix[index] = value;
    }
    public void SetValueReverseIndexMatrix(int index)
    {
        data.matrix[index] *= -1;
    }

    public float GetScore()
    {
        return data.score;
    }
    public void SetScore(float value)
    {
        data.score = value;
    }
    public void SetEarnScore(float value)
    {
        data.score += value;
    }

    public float GetTimeStart()
    {
        return data.timeStart;
    }

    public void SetTimeStart(float value)
    {
        data.timeStart = value;
    }

    public void SetCountTimeStart(float value)
    {
        data.timeStart += value;
    }

    public int GetSpeedBall()
    {
        return data.speedBall;
    }
    public void SetSpeedBall(int value)
    {
        data.speedBall = value;
    }

    public int GetLevelGame()
    {
        return data.levelGame;
    }

    public void SetLevelGame(int value)
    {
        data.levelGame = value;
    }

    public int GetLockCell()
    {
        return data.countLockCell;
    }

    public void SetLockCell()
    {
        data.countLockCell += Defines.VALUE_CHANGE_LOCK_CELL;
        if (data.countLockCell > Defines.MAX_LOCK_CELL)
            data.countLockCell = Defines.MIN_LOCK_CELL;
    }

    public void SetLockCell(int value)
    {
        data.countLockCell = value;        
    }

    //Cache Pool
    public void CreateObjectPool()
    {
        if (!useObjectPool)
            return;

        int count = listObjectsCache.Count;
        for (int i = 0; i < count; i++)
        {
            ObjectPooler.Instance.GetPooledObject(listObjectsCache[i]);
        }
    }

    public GameObject InstantiatePrefab(int indexPrefab)
    {
        GameObject objInit;
            if (!useObjectPool)
            {
                objInit = (GameObject)Instantiate(listObjectsCache[indexPrefab]);
            }
            else
            {
                objInit = ObjectPooler.Instance.GetPooledObject(listObjectsCache[indexPrefab]);
                objInit.SetActive(true);
            }
        return objInit;
    }

    public void DestroyObject(GameObject obj, float timeDelay)
    {
        if (!useObjectPool)
        {
            Destroy(obj, timeDelay);
        }
        else
        {
            obj.SetActive(false);
            obj.transform.SetParent(obj.transform.root.parent);
        }
    }
}

[Serializable]
public class RMSData
{
	public List<float> topTenScore = new List<float>();
	public int[] matrix = new int[Defines.TOTALCELL];
	public float score = 0;
	public float timeStart = 0;
	public int currMode = Defines.MODE_LINES;
	public float soundVolume = 1;
    public int speedBall = 20;
	public int levelGame = 1;
    public int countLockCell = 0;
}
