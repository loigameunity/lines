﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public static AudioManager Instance;

	public AudioClip[] audioClipMusics;
	public AudioClip[] audioClipSFX;
	public float volumeSound{set; get;}
	AudioSource audioSourceMusic;
	AudioSource[] audioSourceSFX = new AudioSource[Defines.MAX_CHANEL_SOUND];

	void Awake()
	{
		if (Instance == null)
			Instance = this;
		DontDestroyOnLoad (gameObject);
	}
	// Use this for initialization
	void Start () {
		audioSourceMusic = gameObject.AddComponent<AudioSource>();
		int count = audioSourceSFX.Length;
		for(int i=0; i<count; i++)
		{
			audioSourceSFX[i] = gameObject.AddComponent<AudioSource>();
		}
		
	}


	public void PlayRandomMusic(bool loop)
	{
		if(GameManager.Instance.GetSoundVolume() > Defines.MIN_SOUND && !audioSourceMusic.isPlaying)
		{
			audioSourceMusic.clip = audioClipMusics[Random.Range(0,audioClipMusics.Length)];
			audioSourceMusic.volume = GameManager.Instance.GetSoundVolume();
			audioSourceMusic.loop = loop;
			audioSourceMusic.Play();
		}
	}

	public void UpdateVolumeMusic()
	{
		if(audioSourceMusic.isPlaying)
		{
			PauseMusic();
			audioSourceMusic.volume = GameManager.Instance.GetSoundVolume();
			UnPauseMusic();
		}
	}

	public void StopMusic()
	{
		if (audioSourceMusic.isPlaying)
			audioSourceMusic.Stop ();
	}

	public bool IsMusicPlaying()
	{
		if (audioSourceMusic.isPlaying)
			return true;
		return false;
	}

	public void PauseMusic()
	{
		audioSourceMusic.Pause ();
	}

	public void UnPauseMusic()
	{
		audioSourceMusic.UnPause ();
	}

	public void PlaySFX(int index)
	{
		if(GameManager.Instance.GetSoundVolume() > Defines.MIN_SOUND)
		{
			if (index < audioClipSFX.Length)
			{
				for (int i = 0; i < Defines.MAX_CHANEL_SOUND; i++)
				{
					if (!audioSourceSFX[i].isPlaying)
					{
						audioSourceSFX[i].clip = audioClipSFX[index];
						audioSourceSFX[i].volume = GameManager.Instance.GetSoundVolume();
						audioSourceSFX[i].Play();
						return;
					}
				}
			}
		}
	}

	public void PlayBreakBallSFX(int resetTime)
	{
        if(resetTime == 0)
            timeDelaySound = -Defines.TIME_DELAY_SOUND_EARN_SCORE;
        StartCoroutine("PlayRandomTime");
	}

    float timeDelaySound = 0;
	IEnumerator PlayRandomTime()
	{
        timeDelaySound += Defines.TIME_DELAY_SOUND_EARN_SCORE;
        yield return new WaitForSeconds(timeDelaySound);      
        PlaySFX(Defines.SFX_BREAK_BALL);
	}

}
