﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour {

	// Use this for initialization
	public List<Sprite> listBall = new List<Sprite>();
	public List<Sprite> listModeSprite = new List<Sprite>();
	public GameObject cellGroupPanel;
	public Text hightScoreTextShow;
	public Text scoreTextShow;
	public Text timeTextShow;
	public GameObject listNextBallshow;
	public GameObject endGamePanel;
	public GameObject boardEndGame;
    public GameObject ballEffectImage;
    public GameObject scoreEffectText;
	public GameObject menuPanel;
	public GameObject dialogPanel;
	public Text dialogContent;
	public GameObject helpPanel;
	public GameObject highScorePanel;
	public GameObject modeButtonImage;
    public Image imageFillSpeedBall;
    public GameObject groupHighScorePanel;
	public Image imageFillSoundVolume;
	public Image imageFillLevelGame;
    public Text lockCellTextShow;
	private int dialogType;
	private List<int> listNextBall = new List<int>();
	private int selectedBall = -1;
	private List<Vector2> listPath = new List<Vector2>();
	private bool gamePause = false;
    private int idCellPlayEffect = -1;
    private int stepEffect = -1;
	private bool refeshAnimSmallBall = false;
    private int currLockCell = 0;

	GameManager gameManager = GameManager.Instance;
	AudioManager audioManager = AudioManager.Instance;
	void Start () {

        gameManager.CreateObjectPool(); // Call Cache Pool

        if (gameManager.GetValueIndexMatrix(0) == Defines.NO_DATA_VALUE) // first launch game
		{
			InitGame();
		}
		else
		{
			UpdateTextScoreShow();
			for(int i=0; i<Defines.TOTALCELL; i++)
			{
				if(gameManager.GetValueIndexMatrix(i)<0)
				{
					listNextBall.Add(-gameManager.GetValueIndexMatrix(i));
				}
			}
			UpdateNextBallShow();
		}

		InitBoard();
	}

	// Update is called once per frame
	void Update ()
	{
		UpdateTime();
        if (idCellPlayEffect >= 0)
        {
            if (stepEffect == -1)
            {
                stepEffect = 0;
                int index = (int)(listPath[stepEffect].x * Defines.BOARD_ROW + listPath[stepEffect].y);
                ballEffectImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, cellGroupPanel.transform.GetChild(0).GetComponent<RectTransform>().rect.width);
                ballEffectImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, cellGroupPanel.transform.GetChild(0).GetComponent<RectTransform>().rect.height);

                cellGroupPanel.transform.GetChild(index).GetChild(0).GetComponent<BallScript>().RandomAnimIdle();
                cellGroupPanel.transform.GetChild(index).GetChild(0).gameObject.SetActive(false);
                ballEffectImage.GetComponent<RectTransform>().localPosition = cellGroupPanel.transform.GetChild(index).GetComponent<RectTransform>().localPosition;
                ballEffectImage.GetComponent<Image>().sprite = listBall[gameManager.GetValueIndexMatrix(selectedBall)];
                ballEffectImage.SetActive(true);
                stepEffect++;
            }
            else
            {
                if (stepEffect < listPath.Count)
                {
                    int index = (int)(listPath[stepEffect].x * Defines.BOARD_ROW + listPath[stepEffect].y);
                    ballEffectImage.GetComponent<RectTransform>().localPosition = Vector3.MoveTowards(ballEffectImage.GetComponent<RectTransform>().localPosition, cellGroupPanel.transform.GetChild(index).GetComponent<RectTransform>().localPosition, gameManager.GetSpeedBall());
                    if (Vector3.Distance(ballEffectImage.GetComponent<RectTransform>().localPosition, cellGroupPanel.transform.GetChild(index).GetComponent<RectTransform>().localPosition) < gameManager.GetSpeedBall())
                    {
                        ballEffectImage.GetComponent<RectTransform>().localPosition = cellGroupPanel.transform.GetChild(index).GetComponent<RectTransform>().localPosition;
                        stepEffect++;
                    }
                }
                else
                {
                    stepEffect = -1;
                    ballEffectImage.SetActive(false);
                    UpdateAfterPlayEffect(idCellPlayEffect);
                }
            }
        }
		// Play random sound BG
		if(!audioManager.IsMusicPlaying())
		{
			audioManager.PlayRandomMusic(false);
		}
	}

	private void InitGame()
	{
		gameManager.SetTimeStart(0);
		gameManager.SetScore(0);
		modeButtonImage.GetComponent<Image>().sprite = listModeSprite[gameManager.GetCurrMode()];
		CreateRanMatrix();
		UpdateNextBallShow();
	}

	private void InitBoard()
	{
		for(int i=0; i<Defines.TOTALCELL; i++)
		{
			int j = i;
            GameObject go = gameManager.InstantiatePrefab(Defines.INDEX_CELL_CACHE);
			int valueCell = gameManager.GetValueIndexMatrix(i);
			if(valueCell != 0)
			{
				if(valueCell > 0)
				{
					go.transform.GetChild(0).GetComponent<Image>().sprite = listBall[valueCell];
                    go.transform.GetChild(0).gameObject.SetActive(true);
                    if (valueCell == Defines.BLOCK_LOCK)
                        go.transform.GetChild(0).GetComponent<BallScript>().PlayBallAnim(Defines.ANIM_BLOCK, 0);
                    else
                        go.transform.GetChild(0).GetComponent<BallScript>().PlayBallAnim(Defines.ANIM_BALL_GROWN_UP,0);
				}
				else
				{
					go.transform.GetChild(1).GetComponent<Image>().sprite = listBall[-valueCell];
					go.transform.GetChild(1).gameObject.SetActive(true);
					go.transform.GetChild(1).GetComponent<SmallBallScript>().PlayAnim();
				}
			}
			go.transform.SetParent(cellGroupPanel.transform);
            go.transform.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
			go.GetComponent<Button>().onClick.AddListener(() => CellSelect(j));
		}
	}

	private void UpdateBoard()
	{
        BallScript ballScript;
		for(int i=0; i<Defines.TOTALCELL; i++)
		{
			int valueCell = gameManager.GetValueIndexMatrix(i);
            ballScript = cellGroupPanel.transform.GetChild(i).GetChild(0).GetComponent<BallScript>();
            if (valueCell > 0)
			{

                if (valueCell == Defines.BLOCK_LOCK)
                {
                    ballScript.PlayBallAnim(Defines.ANIM_BLOCK,0);
                }
                cellGroupPanel.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = listBall[valueCell];
				cellGroupPanel.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                cellGroupPanel.transform.GetChild(i).GetChild(0).GetComponent<BallScript>().DeActiveEffectExplose();
                if (ballScript.GetAnimIndexBall() == Defines.ANIM_EAT_BALL && valueCell == Defines.GHOST_BALL_COLOR)
                    ballScript.RandomAnimIdle();

                cellGroupPanel.transform.GetChild(i).GetChild(0).GetComponent<BallScript>().ResetIndexAnim();
				cellGroupPanel.transform.GetChild(i).GetChild(1).gameObject.SetActive(false);
			}
			else if(valueCell == 0)
			{
                if(ballScript.GetAnimIndexBall() != Defines.ANIM_EAT_BALL)
				    cellGroupPanel.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
				cellGroupPanel.transform.GetChild(i).GetChild(1).gameObject.SetActive(false);
			}
			else if(valueCell < 0)
			{
				cellGroupPanel.transform.GetChild(i).GetChild(1).GetComponent<Image>().sprite = listBall[-valueCell];
				cellGroupPanel.transform.GetChild(i).GetChild(1).gameObject.SetActive(true);
				if(refeshAnimSmallBall)
				{
					cellGroupPanel.transform.GetChild(i).GetChild(1).GetComponent<SmallBallScript>().PlayAnim();
				}
				cellGroupPanel.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
			}
		}
	}

	private void CellSelect(int i)
	{
	//	Debug.Log(" Select cell: "+i + "           selectedBall: "+selectedBall);
		refeshAnimSmallBall = false;
        if (idCellPlayEffect >= 0 || gameManager.GetValueIndexMatrix(i) == Defines.BLOCK_LOCK)
        {
            if(gameManager.GetValueIndexMatrix(i) == Defines.BLOCK_LOCK)
                audioManager.PlaySFX(Defines.SFX_WARNING);
            return;
        }
		if(selectedBall >= 0)
		{
			if(gameManager.GetValueIndexMatrix(i) > 0)
			{
				audioManager.PlaySFX(Defines.SFX_SELECT_BALL);
				cellGroupPanel.transform.GetChild(selectedBall).GetChild(0).GetComponent<BallScript>().RandomAnimIdle();
				selectedBall = i;
				cellGroupPanel.transform.GetChild(selectedBall).GetChild(0).GetComponent<BallScript>().PlayBallAnim(Defines.ANIM_BALL_SELECT,0);
			}
			else
			{
				// Check move
				listPath.Clear();
				listPath = FindPath(selectedBall/Defines.BOARD_ROW, selectedBall%Defines.BOARD_ROW, i/Defines.BOARD_ROW, i%Defines.BOARD_ROW);
	//			Debug.Log(" +++ listPath: "+listPath.Count);
	//			for(int abc=0; abc<listPath.Count; abc++)
	//			{
	//				Debug.Log(" +++ listPath : "+listPath[abc]);
	//			}
				if(listPath.Count > 0)
				{
					audioManager.PlaySFX(Defines.SFX_SELECT_CELL);
                    idCellPlayEffect = i;
					refeshAnimSmallBall = true;
                }
				else
				{
					audioManager.PlaySFX(Defines.SFX_WARNING);
                    // No path
                    cellGroupPanel.transform.GetChild(selectedBall).GetChild(0).GetComponent<BallScript>().RandomAnimIdle();
                    selectedBall = -1;
				}

			}
		}
		else
		{
            if (gameManager.GetValueIndexMatrix(i) > 0)
            {
                audioManager.PlaySFX(Defines.SFX_SELECT_BALL);
                selectedBall = i;
                cellGroupPanel.transform.GetChild(selectedBall).GetChild(0).GetComponent<BallScript>().PlayBallAnim(Defines.ANIM_BALL_SELECT,0);
            }
            else
            {
                // Cell Free
                audioManager.PlaySFX(Defines.SFX_SELECT_CELL);
            }
		}
		UpdateBoard();
		UpdateNextBallShow();
		UpdateTextScoreShow();
	}

    private void UpdateAfterPlayEffect(int i)
    {
        int tempValue = 0;
        if (gameManager.GetValueIndexMatrix(i) < 0)
        {
            tempValue = -gameManager.GetValueIndexMatrix(i);
        }
		gameManager.SetValueIndexMatrix(i,gameManager.GetValueIndexMatrix(selectedBall));
        gameManager.SetValueIndexMatrix(selectedBall, 0);
        cellGroupPanel.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = listBall[gameManager.GetValueIndexMatrix(i)];
     
        if (tempValue > 0)
            AddNextStepColor(1, tempValue);

		if(gameManager.GetCurrMode() == Defines.MODE_LINES)
		{
			//Check Get Point Line
			if (!CheckLines(i / Defines.BOARD_ROW, i % Defines.BOARD_ROW))
			{
				BallGrown();
			}
			else //Save game when earn ball
			{
				gameManager.SaveData();
			}
		}
		else if(gameManager.GetCurrMode() == Defines.MODE_SQUARE)
		{
        	//Check Get Point Quare
        	if(!CheckSquares(i/Defines.BOARD_ROW, i%Defines.BOARD_ROW))
        	{
        		BallGrown();
        	}
			else //Save game when earn ball
			{
				gameManager.SaveData();
			}
		}
        idCellPlayEffect = -1;
        selectedBall = -1;
        UpdateBoard();
        UpdateNextBallShow();
        UpdateTextScoreShow();
    }

    private void UpdateTextScoreShow()
	{
		if(gameManager.GetHighScore() < 10)
		hightScoreTextShow.GetComponent<Text>().text = "00000" + gameManager.GetHighScore();
		else if(gameManager.GetHighScore() < 100)
			hightScoreTextShow.GetComponent<Text>().text = "0000" + gameManager.GetHighScore();
		else if(gameManager.GetHighScore() < 1000)
			hightScoreTextShow.GetComponent<Text>().text = "000" + gameManager.GetHighScore();
		else if(gameManager.GetHighScore() < 10000)
			hightScoreTextShow.GetComponent<Text>().text = "00" + gameManager.GetHighScore();
		else if(gameManager.GetHighScore() < 100000)
			hightScoreTextShow.GetComponent<Text>().text = "0" + gameManager.GetHighScore();
		else
			hightScoreTextShow.GetComponent<Text>().text = "" + gameManager.GetHighScore();

		if(gameManager.GetScore() < 10)
			scoreTextShow.GetComponent<Text>().text = "00000" + gameManager.GetScore();
		else if(gameManager.GetScore() < 100)
			scoreTextShow.GetComponent<Text>().text = "0000" + gameManager.GetScore();
		else if(gameManager.GetScore() < 1000)
			scoreTextShow.GetComponent<Text>().text = "000" + gameManager.GetScore();
		else if(gameManager.GetScore() < 10000)
			scoreTextShow.GetComponent<Text>().text = "00" + gameManager.GetScore();
		else if(gameManager.GetScore() < 100000)
			scoreTextShow.GetComponent<Text>().text = "0" + gameManager.GetScore();
		else
			scoreTextShow.GetComponent<Text>().text = "" + gameManager.GetScore();

	}

	private void UpdateTime()
	{
		if(gamePause)
			return;
		if(Time.deltaTime < 1)
			gameManager.SetCountTimeStart(Time.deltaTime);
		int second = (int)gameManager.GetTimeStart();
		int hour = second/3600;
		string hourString = hour < 10 ? ("0"+hour) : ("" + hour);
		string minString = (second/60)%60 < 10 ? ("0" + (second/60)%60) : (""+(second/60)%60);
		string secondString = second%60 < 10 ?  ("0" + second%60) : ("" +second%60);
		timeTextShow.GetComponent<Text>().text = hourString+":" + minString + ":" + secondString;
	}

	private void BallGrown()
	{
	//	Debug.Log(" +++++ Ball Grown ++++++");
		for(int i=0; i<Defines.TOTALCELL; i++)
		{
			if(gameManager.GetValueIndexMatrix(i) < 0)
			{
				//Play Effect Ground
				gameManager.SetValueReverseIndexMatrix(i);
                cellGroupPanel.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = listBall[gameManager.GetValueIndexMatrix(i)];
                if (gameManager.GetCurrMode() == Defines.MODE_LINES)
                    CheckLines(i/Defines.BOARD_ROW, i%Defines.BOARD_ROW);
                else if(gameManager.GetCurrMode() == Defines.MODE_SQUARE)
                    CheckSquares(i / Defines.BOARD_ROW, i % Defines.BOARD_ROW);
            }
		}
		AddNextStepColor(Defines.NEXT_BALL_NUM, Defines.RANDOM_BALL);
	}

	private void UpdateNextBallShow()
	{
		int count = listNextBall.Count;
		if(count > 0)
		{
			for(int i=0; i<count; i++)
			{
				listNextBallshow.transform.GetChild(i+1).GetComponent<Image>().sprite = listBall[listNextBall[i]];
			}
		}
	}

	private int CellRandom(int n)
	{
		return Random.Range(0,n);
	}

	private void CreateRanMatrix()
	{
        int i, j;

		for(i=0; i<Defines.BOARD_ROW; i++)
		{
			for(j=0; j<Defines.BOARD_COL; j++)
			{
				gameManager.SetValueIndexMatrix(i*Defines.BOARD_ROW + j, 0);
			}
		}

        if(gameManager.GetLockCell() > 0)
            CreateValueCell(gameManager.GetLockCell(), Defines.BLOCK_LOCK);
        CreateValueCell(Defines.INIT_BALL_NUM, Defines.RANDOM_BALL);
        AddNextStepColor(Defines.NEXT_BALL_NUM, Defines.RANDOM_BALL);
	}

    private void CreateValueCell(int countCreate, int valueColor)
    {
        int count = CountEmpty();
        int count2 = count - countCreate;

        int nextStep, i,j;
        bool stop;
        do
        {
            nextStep = CellRandom(count--) + 1;
            stop = false;
            for (i = 0; i < Defines.BOARD_ROW; i++)
            {
                if (stop) break;
                for (j = 0; j < Defines.BOARD_COL; j++)
                {
                    if (gameManager.GetValueIndexMatrix(i * Defines.BOARD_ROW + j) == 0)
                    {
                        nextStep--;
                        if (nextStep == 0)
                        {
                            gameManager.SetValueIndexMatrix(i * Defines.BOARD_ROW + j, valueColor == Defines.RANDOM_BALL?  (CellRandom(Defines.MAX_COLOR) + 1):valueColor);
                            stop = true;
                            break;
                        }
                    }
                }
            }
        } while (count > count2);
    }

	private void AddNextStepColor(int numBall, int id)
	{
		int count, tmp, i, j, nextStep;
		bool stop;
		count = CountEmpty();

		if(count >= Defines.NEXT_BALL_NUM)
		{
			listNextBall.Clear();
			for (tmp = 0; tmp < numBall; tmp++)
			{
				nextStep = CellRandom(count--) + 1;
				stop = false;
				for (i = 0; i <Defines.BOARD_ROW; i++)
				{
					if (stop) break;
					for (j = 0; j <Defines.BOARD_COL; j++)
					{
						if (gameManager.GetValueIndexMatrix(i*Defines.BOARD_ROW + j) == 0)
						{
							nextStep--;
							if (nextStep == 0)
							{
								int cellValue;
								if(id > Defines.RANDOM_BALL)
									cellValue = id;
								else
									cellValue = CellRandom(Defines.MAX_COLOR) + 1;
								
								int countSameColor = CountSameColor(i,j,cellValue);
								if(CalculatePassLevelGame(countSameColor) || numBall > Defines.MAX_RETRY_RANDOM_BALL || CountEmpty() <= Defines.MIN_CELL_BLANK)
								{
									gameManager.SetValueIndexMatrix(i*Defines.BOARD_ROW + j, - (cellValue));
									listNextBall.Add(cellValue);
								}
								else
								{
									numBall ++;
									count ++;
								}
								stop = true;
								break;
							}
						}
					}
				}
			}
		}
		else
		{
			// Lose Game
			gamePause = true;
			endGamePanel.SetActive(true);
			UpdateEndgame();
		}
	}

	private bool CalculatePassLevelGame(int countSame)
	{
		if(countSame <= Defines.CONDITION_LEVEL[gameManager.GetLevelGame() - Defines.MIN_LEVEL_GAME])
			return true;
		else
			return false;
	}
	private int CountSameColor(int iCenter, int jCenter, int cellValue)
	{
		int count = 0;

		int[] u = {-1, 0, 1,-1, 1,-1, 0, 1};
		int[] v = {-1,-1,-1, 0, 0, 1, 1, 1};
		int i, j, t;
		
		i = iCenter;
		j = jCenter;
		for (t = 0; t < 8; t++)
		{	
			i = iCenter + u[t];
			j = jCenter + v[t];
			if (IsInside(i, j) && gameManager.GetValueIndexMatrix(i*Defines.BOARD_ROW + j) == cellValue)
			{
				count ++;
			}
		}

		return count;
	}

	private void UpdateEndgame()
	{
		gameManager.SetHighScore(gameManager.GetScore());
		int second = (int)gameManager.GetTimeStart();
		int hour = second/3600;
		string hourString = hour < 10 ? ("0"+hour) : ("" + hour);
		string minString = (second/60)%60 < 10 ? ("0" + (second/60)%60) : (""+(second/60)%60);
		string secondString = second%60 < 10 ?  ("0" + second%60) : ("" +second%60);
		boardEndGame.transform.GetChild(1).GetComponent<Text>().text = hourString+":" + minString + ":" + secondString;
		
		boardEndGame.transform.GetChild(3).GetComponent<Text>().text = "" + gameManager.GetScore();
		boardEndGame.transform.GetChild(5).GetComponent<Text>().text = "" + gameManager.GetHighScore();
	}

	public void NextGame()
	{
		gamePause = false;
		endGamePanel.SetActive(false);
		InitGame();
		UpdateBoard();
		UpdateTextScoreShow();
        gameManager.SaveData();
	}

	private int CountEmpty()
	{
		int i, j, count;
		count = 0;
		for (i = 0; i <Defines.BOARD_COL; i++)
			for (j = 0; j <Defines.BOARD_ROW; j++)
				if (gameManager.GetValueIndexMatrix(i*Defines.BOARD_COL + j) <= 0)
					count++;
		return count;
	}

	private List<Vector2> FindPath(int fromI, int fromJ, int toI, int toJ)
	{
		int[] matrixI = new int[Defines.TOTALCELL];
		int[] matrixJ = new int[Defines.TOTALCELL];
	
		int[] queueI = new int[Defines.TOTALCELL];
		int[] queueJ = new int[Defines.TOTALCELL];
	
		int[] u = {1, 0, -1, 0};
		int[] v = {0, 1, 0, -1};
	
		int fist = 0, last = 0;
	
		int x, y, xx, yy, k;
	
		List<Vector2> listPath = new List<Vector2>();
	
		for (x = 0; x <Defines.BOARD_ROW; x++)
		{
			for (y = 0; y <Defines.BOARD_COL; y++)
			{
				matrixI[x*Defines.BOARD_ROW + y] = -1;
			}
		}

		queueI[0] = toI;
		queueJ[0] = toJ;
		matrixI[toI*Defines.BOARD_ROW + toJ] = -2;
	
		while (fist <= last)
		{
			x = queueI[fist];
			y = queueJ[fist];
			fist++;
			for (k = 0; k < 4; k++)
			{
				xx = x + u[k];
				yy = y + v[k];
				if (xx == fromI && yy == fromJ)
				{
					matrixI[fromI*Defines.BOARD_ROW + fromJ] = x;
					matrixJ[fromI*Defines.BOARD_ROW + fromJ] = y;

					bool stop = false;
					while (!stop)
					{
						listPath.Add(new Vector2(fromI, fromJ));
						k = fromI;
						fromI = matrixI[fromI * Defines.BOARD_ROW + fromJ];
						if (fromI == -2)
							stop = true;

						fromJ = matrixJ[k*Defines.BOARD_ROW + fromJ];
					}
					return listPath;
				}
	
				if (!IsInside(xx, yy)) continue;
	
				if (matrixI[xx*Defines.BOARD_ROW + yy] == -1 && (gameManager.GetValueIndexMatrix(xx*Defines.BOARD_ROW + yy) <= 0|| gameManager.GetValueIndexMatrix(fromI*Defines.BOARD_ROW + fromJ) == Defines.GHOST_BALL_COLOR))
				{
					last++;
					queueI[last] = xx;
					queueJ[last] = yy;
					matrixI[xx*Defines.BOARD_ROW + yy] = x;
					matrixJ[xx*Defines.BOARD_ROW + yy] = y;
				}
			}
		}
	
		return listPath;
	}
	
	private bool IsInside(int i, int j)
	{
		return (i >= 0 && i <Defines.BOARD_ROW && j >= 0 && j <Defines.BOARD_COL);
	}

	bool CheckLines(int iCenter, int jCenter)
	{
	//	Debug.Log(" ++++ Check Lines ++++");
		List<Vector2> lines = new List<Vector2>();
	
		int[] u = {0, 1, 1, 1};
		int[] v = {1, 0, -1, 1};
		int i, j, k, t, extra, colorBall;
		int earnScore = 0;
		int extraGoshtBall = 2;
		if(gameManager.GetValueIndexMatrix(iCenter*Defines.BOARD_ROW + jCenter) == Defines.GHOST_BALL_COLOR)
			extraGoshtBall = Defines.GHOST_BALL_COLOR;

		for(extra = extraGoshtBall - 1; extra > 0; extra--)
		{
			colorBall = gameManager.GetValueIndexMatrix(iCenter*Defines.BOARD_ROW + jCenter);
			if(colorBall == Defines.GHOST_BALL_COLOR)
				colorBall = extra;
			for (t = 0; t < 4; t++)
			{
				k = 0;
				i = iCenter;
				j = jCenter;
				bool stop = false;
				while (!stop)
				{
					i += u[t];
					j += v[t];
					if (!IsInside(i, j))
					{
						stop = true;
						break;
					}
					if (gameManager.GetValueIndexMatrix(i*Defines.BOARD_ROW + j) != colorBall
					&& gameManager.GetValueIndexMatrix(i*Defines.BOARD_ROW + j) != Defines.GHOST_BALL_COLOR
					)
					{
						stop = true;
						break;
					}
					k++;
				}

				i = iCenter;
				j = jCenter;
				stop = false;
				while (!stop)
				{
					i -= u[t];
					j -= v[t];
					if (!IsInside(i, j))
					{
						stop = true;
						break;
					}

					if (gameManager.GetValueIndexMatrix(i*Defines.BOARD_ROW + j) != colorBall
						&& gameManager.GetValueIndexMatrix(i*Defines.BOARD_ROW + j) != Defines.GHOST_BALL_COLOR
					)
					{
						stop = true;
						break;
					}
					k++;
				}
				k++;
				if (k >= Defines.EAT_BALL_LINE_NUM)
				{
					while (k-- > 0)
					{
						i += u[t];
						j += v[t];
						if (i != iCenter || j != jCenter)
						{
							lines.Add(new Vector2(i,j));
						}
					}
				}
			}
		
			earnScore = lines.Count;
		//	Debug.Log(" +++ earnScore: "+earnScore);
			if (earnScore > 0)
			{
				lines.Add(new Vector2(iCenter, jCenter));
				earnScore = lines.Count;
				gameManager.SetEarnScore(earnScore);
				for(i=0; i<earnScore; i++)
				{
					// effect earn ball
					int indexReset = (int)(lines[i].x * Defines.BOARD_ROW + lines[i].y);
					
                    cellGroupPanel.transform.GetChild(indexReset).GetChild(0).gameObject.SetActive(true);
                    cellGroupPanel.transform.GetChild(indexReset).GetChild(0).GetComponent<BallScript>().PlayBallAnim(Defines.ANIM_EAT_BALL,gameManager.GetValueIndexMatrix(indexReset));
				//	cellGroupPanel.transform.GetChild(indexReset).GetChild(0).gameObject.SetActive(false); //play anim and deactive
					cellGroupPanel.transform.GetChild(indexReset).GetChild(1).gameObject.SetActive(false);
                    gameManager.SetValueIndexMatrix(indexReset, 0);
                    audioManager.PlayBreakBallSFX(i);
					
				}
				
				// Feature Ghost Ball
				if(earnScore >= Defines.CHECK_ADD_GHOST_BALL)
				{
					//Create Ghost Ball when eat 6 and more ball
					gameManager.SetValueIndexMatrix(iCenter*Defines.BOARD_ROW + jCenter, Defines.GHOST_BALL_COLOR);
				}

                scoreEffectText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, cellGroupPanel.transform.GetChild(0).GetComponent<RectTransform>().rect.width);
                scoreEffectText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, cellGroupPanel.transform.GetChild(0).GetComponent<RectTransform>().rect.height);
                scoreEffectText.GetComponent<RectTransform>().localPosition = cellGroupPanel.transform.GetChild(iCenter*Defines.BOARD_ROW+jCenter).GetComponent<RectTransform>().localPosition;
                scoreEffectText.GetComponent<Text>().text = "+" + earnScore;
                scoreEffectText.SetActive(true);
                Invoke("DeactiveScoreEffectText",2);

                break;
			}
		}

		return earnScore > 0 ? true : false;
	}

    private void DeactiveScoreEffectText()
    {
        scoreEffectText.SetActive(false);
    }
	bool CheckSquares(int iCenter, int jCenter)
	{
		List<Vector2> squares = new List<Vector2>();
	
		int[] u = {-1, 0, 1, 1, 1, 0, -1, -1};
		int[] v = {-1, -1, -1, 0, 1, 1, 1, 0};
		int[] mark = {0, 0, 0, 0, 0, 0, 0, 0};
		int[,] eatRect = new int[8,5]
		{
			{1, 2, 3,  4,  5},
			{3, 4, 5,  6,  7},
			{5, 6, 7,  0,  1},
			{7, 0, 1,  2,  3},
			{1, 2, 3, -1, -1},
			{3, 4, 5, -1, -1},
			{5, 6, 7, -1, -1},
			{7, 0, 1, -1, -1}
		};
	
		int earnScore = 0;
		int extraGoshtBall = 2;
		int extra, colorBall;

		if(gameManager.GetValueIndexMatrix(iCenter*Defines.BOARD_ROW + jCenter) == Defines.GHOST_BALL_COLOR)
            extraGoshtBall = Defines.GHOST_BALL_COLOR;

        for (extra = extraGoshtBall - 1; extra > 0; extra--)
        {
			colorBall = gameManager.GetValueIndexMatrix(iCenter*Defines.BOARD_ROW + jCenter);
			if(colorBall == Defines.GHOST_BALL_COLOR)
				colorBall = extra;
				
			int x, y, k, i, j;
            for (k = 0; k < 8; k++)
			{
				x = iCenter + u[k];
				y = jCenter + v[k];
                mark[k] = 0;
                if (IsInside(x, y) && (gameManager.GetValueIndexMatrix(x*Defines.BOARD_ROW + y) == colorBall || gameManager.GetValueIndexMatrix(x*Defines.BOARD_ROW + y) == Defines.GHOST_BALL_COLOR))
				{
					mark[k] = 1;
				}
			}
		
			for (k = 0; k < 8; k++)
			{
				x = k <= 3 ? 5 : 3;
				j = 1;
				for (i = 0; i < 5; i++)
				{
					int value = eatRect[k,i];
					if (value != -1 && mark[value] == 0)
					{
						j = 0;
						break;
					}
				}
				if (j == 1)
				{
					squares.Add(new Vector2(iCenter, jCenter));
					for (i = 0; i < x; i++)
					{
						squares.Add(new Vector2(iCenter + u[eatRect[k,i]], jCenter + v[eatRect[k,i]]));
					}
				}
			}

			earnScore = squares.Count;

			if(earnScore > 0)
			{
				if(earnScore > 4)
					earnScore = 6;
			
				gameManager.SetEarnScore(earnScore);
				for(i=0; i<earnScore; i++)
				{
					// effect earn ball
					int indexReset = (int)(squares[i].x * Defines.BOARD_ROW + squares[i].y);
					
                    cellGroupPanel.transform.GetChild(indexReset).GetChild(0).gameObject.SetActive(true);
                    cellGroupPanel.transform.GetChild(indexReset).GetChild(0).GetComponent<BallScript>().PlayBallAnim(Defines.ANIM_EAT_BALL, gameManager.GetValueIndexMatrix(indexReset));
                    //    cellGroupPanel.transform.GetChild(indexReset).GetChild(0).gameObject.SetActive(false);
                    cellGroupPanel.transform.GetChild(indexReset).GetChild(1).gameObject.SetActive(false);
                    gameManager.SetValueIndexMatrix(indexReset, 0);
                    audioManager.PlayBreakBallSFX(i);
				}

				// Feature Ghost Ball
				if(earnScore >= Defines.CHECK_ADD_GHOST_BALL)
				{
					//Create Ghost Ball when eat 6 and more ball
					gameManager.SetValueIndexMatrix(iCenter*Defines.BOARD_ROW + jCenter, Defines.GHOST_BALL_COLOR);
				}

                scoreEffectText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, cellGroupPanel.transform.GetChild(0).GetComponent<RectTransform>().rect.width);
                scoreEffectText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, cellGroupPanel.transform.GetChild(0).GetComponent<RectTransform>().rect.height);
                scoreEffectText.GetComponent<RectTransform>().localPosition = cellGroupPanel.transform.GetChild(iCenter * Defines.BOARD_ROW + jCenter).GetComponent<RectTransform>().localPosition;
                scoreEffectText.GetComponent<Text>().text = "+" + earnScore;
                scoreEffectText.SetActive(true);
                Invoke("DeactiveScoreEffectText", 2);

                break;
			}
		}

		return earnScore > 0 ? true : false;
	}
	// Menu Button Code
	public void ModeButton()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		gamePause = true;
		dialogType = Defines.DIALOG_TYPE_MODE;
		if(gameManager.GetCurrMode() == Defines.MODE_LINES)
			dialogContent.GetComponent<Text>().text = Defines.DIALOG_CHANGE_MODE_SQUARE;
		else if(gameManager.GetCurrMode() == Defines.MODE_SQUARE)
			dialogContent.GetComponent<Text>().text = Defines.DIALOG_CHANGE_MODE_LINES;
		
		dialogPanel.gameObject.SetActive(true);
	}
	public void MenuShow()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		gamePause = true;
        currLockCell = gameManager.GetLockCell();
        imageFillSpeedBall.GetComponent<Image>().fillAmount = (float)gameManager.GetSpeedBall() / Defines.MAX_SPEED_BALL;
		imageFillSoundVolume.GetComponent<Image>().fillAmount = (float)gameManager.GetSoundVolume() / Defines.MAX_SOUND;
		imageFillLevelGame.GetComponent<Image>().fillAmount = (float)gameManager.GetLevelGame() / Defines.MAX_LEVEL_GAME;
        if (gameManager.GetLockCell() == Defines.MIN_LOCK_CELL)
            lockCellTextShow.GetComponent<Text>().text = "No Lock Cell";
        else
            lockCellTextShow.GetComponent<Text>().text = "Lock " + gameManager.GetLockCell() + " Cells";
        menuPanel.SetActive(true);
	}

	public void NewGame()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		dialogType = Defines.DIALOG_TYPE_NEWGAME;
		dialogContent.GetComponent<Text>().text = Defines.DIALOG_NEWGAME;
		dialogPanel.gameObject.SetActive(true);
	}

	public void HelpShow()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		helpPanel.SetActive(true);
	}
	public void HighScoreShow()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
        for (int i = 0; i < Defines.TOTAL_HIGHSCORE_RMS; i++)
        {
            if (gameManager.GetHighScore(i) > 0)
            {
                GameObject go = gameManager.InstantiatePrefab(Defines.INDEX_HIGHSCORE_CACHE);
                go.transform.GetChild(0).GetComponent<Text>().text = i + 1 + ".";
                go.transform.GetChild(1).GetComponent<Text>().text = "" + gameManager.GetHighScore(i);
                go.transform.SetParent(groupHighScorePanel.transform);
                go.transform.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            }
        }

        highScorePanel.SetActive(true);
	}
	public void YesConfirm()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
        if (dialogType == Defines.DIALOG_TYPE_NEWGAME)
        {
            dialogPanel.gameObject.SetActive(false);
            menuPanel.gameObject.SetActive(false);
            gamePause = false;
            NextGame();
        }
        else if (dialogType == Defines.DIALOG_TYPE_EXIT)
        {
            //SaveGame
            gameManager.SaveData();
            Application.Quit();
        }
        else if (dialogType == Defines.DIALOG_TYPE_MODE)
        {
            if (gameManager.GetCurrMode() == Defines.MODE_LINES)
                gameManager.SetCurrMode(Defines.MODE_SQUARE);
            else if (gameManager.GetCurrMode() == Defines.MODE_SQUARE)
                gameManager.SetCurrMode(Defines.MODE_LINES);

            modeButtonImage.GetComponent<Image>().sprite = listModeSprite[gameManager.GetCurrMode()];
            dialogPanel.gameObject.SetActive(false);
            gamePause = false;
            NextGame();
        }
        else if (dialogType == Defines.DIALOG_TYPE_CHANGE_LOCK_CELL)
        {
            dialogPanel.gameObject.SetActive(false);
            gamePause = false;
            menuPanel.SetActive(false);
            NextGame();
            currLockCell = gameManager.GetLockCell();
            gameManager.SaveData();
        }

    }

	public void NoConfirm()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		dialogPanel.gameObject.SetActive(false);
        if (dialogType == Defines.DIALOG_TYPE_CHANGE_LOCK_CELL)
        {
            gameManager.SetLockCell(currLockCell);
            gamePause = false;
            menuPanel.SetActive(false);
            gameManager.SaveData();
        }

	}

	public void BackFromHelp()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		helpPanel.SetActive(false);
	}

	public void BackFromMenu()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
	
        if (currLockCell != gameManager.GetLockCell())
        {
            dialogType = Defines.DIALOG_TYPE_CHANGE_LOCK_CELL;
            dialogContent.GetComponent<Text>().text = Defines.DIALOG_CHANGE_LOCK_CELL;
            dialogPanel.gameObject.SetActive(true);
        }
        else
        {
            gamePause = false;
            menuPanel.SetActive(false);
            gameManager.SaveData();
        }
	}

	public void BackFromHighScore()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
        int count = groupHighScorePanel.transform.childCount;
        for (int n = count-1; n >=0 ; n--)
        {
            gameManager.DestroyObject(groupHighScorePanel.transform.GetChild(n).gameObject, 0);
            //    Destroy(groupHighScorePanel.transform.GetChild(n).gameObject,0.05f);
        }
        highScorePanel.SetActive(false);
	}

    public void SpeedBallChange(int value)
    {
		audioManager.PlaySFX(Defines.SFX_MENU);
        gameManager.SetSpeedBall(value);
        imageFillSpeedBall.GetComponent<Image>().fillAmount = (float)gameManager.GetSpeedBall() / Defines.MAX_SPEED_BALL;
    }
	public void SoundVolumeChange(float value)
    {
		audioManager.PlaySFX(Defines.SFX_MENU);
        gameManager.SetSoundVolume(value);
        imageFillSoundVolume.GetComponent<Image>().fillAmount = (float)gameManager.GetSoundVolume() / Defines.MAX_SOUND;
    }
	public void GameLevelChange(int value)
    {
		audioManager.PlaySFX(Defines.SFX_MENU);
        gameManager.SetLevelGame(value);
        
        imageFillLevelGame.GetComponent<Image>().fillAmount = (float)gameManager.GetLevelGame() / Defines.MAX_LEVEL_GAME;
    }

    public void LockCellChange()
    {
        gameManager.SetLockCell();
        if (gameManager.GetLockCell() == Defines.MIN_LOCK_CELL)
            lockCellTextShow.GetComponent<Text>().text = "No Lock Cell";
        else
            lockCellTextShow.GetComponent<Text>().text = "Lock " + gameManager.GetLockCell() + " Cells";
    }
	public void ExitGame()
	{
		audioManager.PlaySFX(Defines.SFX_MENU);
		dialogType = Defines.DIALOG_TYPE_EXIT;
		dialogContent.GetComponent<Text>().text = Defines.DIALOG_EXIT;
		dialogPanel.gameObject.SetActive(true);
	}
}
